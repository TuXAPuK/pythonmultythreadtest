import time


class Logger:

    @staticmethod
    def log(line):
        print("%s %s" % (Logger.__time_str(), line))

    @staticmethod
    def __time_str():
        local_t = time.localtime()
        local_tz = -(time.altzone if local_t.tm_isdst else time.timezone)
        return time.strftime("%Y%m%d%H%M%S", local_t) + \
            ("Z" if local_tz == 0 else "+" if local_tz > 0 else "-") + \
            time.strftime("%H'%M'", time.gmtime(abs(local_tz)))
