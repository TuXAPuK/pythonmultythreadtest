from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn
from .logger import Logger
# import threading
import socket
import ssl


routes = []

class RouteHelper:

    def __init__(self, re):
        self.re = re

    def callRoute(self, cbfunc):
        self.callback = cbfunc


def route(re):
    callie = RouteHelper(re)
    routes.append(callie)
    return callie.callRoute





class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    pass


class HttpRouter:

    def add_route(self, reflection_class, calle):
        pass


class HttpServer:

    def __init__(self, port: int = 8044, withssl: bool = False, sslkey: str = None, sslcert: str = None):
        self.withssl = withssl
        self.port = port
        self.server = ThreadingSimpleServer(('0.0.0.0', port), MyHandler)
        if withssl:
            self.server.socket = ssl.wrap_socket(self.server.socket, keyfile=sslkey, certfile=sslcert, server_side=True)

    def start(self):
        hostname = socket.gethostname()
        Logger.log("Starting server on http%s://%s:%d/" % (('s' if self.withssl else ''), hostname, self.port))
        self.server.serve_forever()


class MyHandler(BaseHTTPRequestHandler):

    def do_HEAD(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        paths = {
            '/': {'status': 200},
            '/foo': {'status': 200},
            '/bar': {'status': 302},
            '/baz': {'status': 404},
            '/qux': {'status': 500}
        }
        if self.path in paths:
            self.respond(paths[self.path])
        else:
            self.respond({'status': 500})

    def handle_http(self, status_code, path):
        self.send_response(status_code)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        content = '''
        <html><head><title>Title goes here.</title></head>
        <body><p>This is a test.</p>
        <p>You accessed path: {}</p>
        <p><strong>Routes:</strong>
        <table border="1" width="100%">'''.format(path)
        for k in routes:
            content += "<tr><td>%s</td><td>%s</td><td>%s</td></tr>" % (k.re, str(k.callback), str(type(str(k.callback.__qualname__), (), {'__tablename__': k.re})))
            print(k.re, str(k.callback), str(type(str(k.callback.__qualname__), (), {'__tablename__': k.re})))

        content += '''</table></p></body></html>'''
        return bytes(content, 'UTF-8')

    def respond(self, opts):
        response = self.handle_http(opts['status'], self.path)
        self.wfile.write(response)
