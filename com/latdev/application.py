import sys
from .httpserver import HttpServer, route


class Application:

    def __init__(self):
        port = 8044
        if sys.argv[1:]:
            port = int(sys.argv[1])
        self.server = HttpServer(port=port)

    def start(self):
        self.server.start()

class Index:

    @route(r"^/$")
    def action_main(self):
        print("/index")

    @route(r"^/about$")
    def action_about(self):
        print("/about")
